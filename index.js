document.getElementById('top-left').onclick = function changeContent() {
    var children = this.childNodes[1].getElementsByClassName('large-cell-content');
    for (var i = 0; i < children.length; i++) {
        children[i].classList.add('active');
    }

    // document.getElementById('top-left').childNodes.style = "background-color: red;";
}

var smallCells = document.getElementsByClassName('small-cell');
var textTable = document.getElementById('text-par');

for (var i = 0; i < smallCells.length; i++) {
    var anchor = smallCells[i]
    anchor.onclick = function addToText() {
        textTable.innerText += this.innerText;
    }
}